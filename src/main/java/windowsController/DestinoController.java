package windowsController;

import vuelos.Ciudad;

public class DestinoController {

    private Ciudad destinoSeleccionado;


    public Ciudad getDestinoSeleccionado() {
        return destinoSeleccionado;
    }

    public void setDestinoSeleccionado(Ciudad destinoSeleccionado) {
        this.destinoSeleccionado = destinoSeleccionado;
    }
}
