package windowsController;

import avion.Avion;

public class AvionController {

    private Avion avionSeleccionado;

    public Avion getAvionSeleccionado() {
        return avionSeleccionado;
    }

    public void setAvionSeleccionado(Avion avionSeleccionado) {
        this.avionSeleccionado = avionSeleccionado;
    }
}
