package windowsController;

import ventaDelPasaje.Venta;

public class VentaController {

    private Venta ventaSeleccionada;


    public Venta getVentaSeleccionada() {
        return ventaSeleccionada;
    }

    public void setVentaSeleccionada(Venta ventaSeleccionada) {
        this.ventaSeleccionada = ventaSeleccionada;
    }
}
