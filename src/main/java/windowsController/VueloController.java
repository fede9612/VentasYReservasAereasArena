package windowsController;

import org.uqbar.commons.utils.Observable;

import vuelos.Vuelo;

@Observable
public class VueloController {
	
	private Vuelo vueloSeleccionado;
	
	public Vuelo getVueloSeleccionado(){
		return this.vueloSeleccionado;
	}
	
	public void setVueloSeleccionado(Vuelo vueloSeleccionado){
		this.vueloSeleccionado = vueloSeleccionado;
	}
}
