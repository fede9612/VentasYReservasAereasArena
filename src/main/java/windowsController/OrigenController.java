package windowsController;

import vuelos.Ciudad;

public class OrigenController {

    private Ciudad origenSeleccionado;


    public Ciudad getOrigenSeleccionado() {
        return origenSeleccionado;
    }

    public void setOrigenSeleccionado(Ciudad origenSeleccionado) {
        this.origenSeleccionado = origenSeleccionado;
    }
}
