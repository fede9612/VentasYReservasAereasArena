package ventanas;

import org.uqbar.arena.bindings.ObservableProperty;
import org.uqbar.arena.filters.TextFilter;
import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.TextBox;
import org.uqbar.arena.windows.Window;
import org.uqbar.arena.windows.WindowOwner;
import pasaje.Pasaje;
import pasajeroPersona.Pasajero;
import ventaDelPasaje.Venta;
import vuelos.Vuelo;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;

public class AgregarPasajeroWindow extends Window<Pasajero>{

    private Vuelo vuelo;
    private DetalleVueloWindows windowDetalle;
    private Venta venta;

    public AgregarPasajeroWindow(WindowOwner owner, Pasajero model, Vuelo vuelo, DetalleVueloWindows windowDetalle) {
        super(owner, model);
        this.vuelo = vuelo;
        this.windowDetalle = windowDetalle;
    }

    @Override
    public void createContents(Panel panel) {

        new Label(panel)
                .setText("Agregar Pasajero")
                .setFontSize(12)
                .setForeground(Color.blue);

        Panel panelDatos = new Panel(panel);
        this.panelDatosDelPasajeroYVuelo(panelDatos);

        Button aceptar = new Button(panel);
        aceptar.setCaption("Agregar");
        aceptar.onClick(() -> {
            this.chequeoYAgregarPasajero();
        });

        Button cancelar = new Button(panel);
        cancelar.setCaption("Cancelar");
        cancelar.onClick(() -> {
            this.close();
        });
    }

    private void panelDatosDelPasajeroYVuelo(Panel panel) {
        panel.setLayout(new ColumnLayout(2));
        new Label(panel)
                .setText("Ingrese DNI: ");
        TextBox txtDni = new TextBox(panel);
        txtDni.setWidth(80);
        txtDni.withFilter(TextFilter.NUMERIC_TEXT_FILTER);
        txtDni.bindValueToProperty("dni");
        new Label(panel)
                .setText("Ingrese Nombre completo: ");
        TextBox txtNombre = new TextBox(panel);
        txtNombre.setWidth(150);
        txtNombre.bindValueToProperty("nomApe");
        new Label(panel)
                .setText("Fecha de compra: ");
        new Label(panel)
                .setText(LocalDate.now().toString());
        new Label(panel)
                .setText("Precio del vuelo: ");
        new Label(panel)
                .bindValue(new ObservableProperty<>(this.vuelo, "precio"));
    }

    private void chequeoYAgregarPasajero() {
        if (this.noHayDatos()){
            JOptionPane.showMessageDialog(null,"Debe ingresar nombre completo y DNI",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else if(this.vuelo.verificarDisponibilidad()){
            //Ac� la verificacion si hay disponible lo tuve que hacer con un if y no con un try
            //Porque arena me tiraba uno de sus errores raros, este error era un runtimeExeption
            //entonces si atrapaba el error dentro de un try catch, cada vez que agregaba un pasajero
            //Me aparecia el mensaje de que no estaba disponible para la venta, gracias al error de arena
            venta = new Venta(this.vuelo,new Pasaje(this.vuelo),this.getModelObject());
            venta.setFechaDeCompra(LocalDate.now());
            this.windowDetalle.actualizarEstados();
            this.close();
        }else{
            JOptionPane.showMessageDialog(null,"No se puede vender m�s pasajes",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private boolean noHayDatos() {
        return this.getModelObject().getDni() == null || this.getModelObject().getNomApe() == null;
    }
}
