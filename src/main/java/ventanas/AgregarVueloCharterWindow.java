package ventanas;

import empresa.Empresa;
import org.uqbar.arena.bindings.ObservableProperty;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.TextBox;
import org.uqbar.arena.windows.Window;
import org.uqbar.arena.windows.WindowOwner;
import vuelos.*;
import windowsController.AvionController;
import windowsController.DestinoController;
import windowsController.OrigenController;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;

public class AgregarVueloCharterWindow extends Window<Vuelo>{

    private AvionController avionSeleccionado = new AvionController();
    private DestinoController destinoSeleccionado = new DestinoController();
    private OrigenController origenSeleccionado = new OrigenController();
    private Integer dia;
    private Integer mes;
    private Integer anio;
    private AgregarVuelosWindow agregarWindow;
    private int cantAsientosOcupados;

    public AgregarVueloCharterWindow(WindowOwner owner, Vuelo model, AgregarVuelosWindow window) {
        super(owner, model);
        this.agregarWindow = window;
    }

    @Override
    public void createContents(Panel mainPanel) {
        new Label(mainPanel)
                .setText("Agregar nuevo Vuelo Charter")
                .setFontSize(12)
                .setForeground(Color.blue);

        agregarWindow.panelFechas(mainPanel);

        agregarWindow.panelSeleccionarAvion(mainPanel, avionSeleccionado);

        agregarWindow.origenYDestinoPanel(mainPanel, origenSeleccionado, destinoSeleccionado);

        new Label(mainPanel)
                .setText("Cantidad de asientos ocupados");
        TextBox txtAsientosOcupados = new TextBox(mainPanel);
        txtAsientosOcupados.bindValue(new ObservableProperty<Object>(this, "cantAsientosOcupados"));

        org.uqbar.arena.widgets.Button aceptarBtn = new Button(mainPanel);
        aceptarBtn.setCaption("Aceptar");
        aceptarBtn.onClick(() ->{
            this.chequeoYAgregaVueloCharter();
        });
    }

    private void chequeoYAgregaVueloCharter() {
        if(this.agregarWindow.noHayFechas()){
            JOptionPane.showMessageDialog(null,"Debe ingresar la fecha",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else if(this.agregarWindow.noHayAvion(avionSeleccionado)){
            JOptionPane.showMessageDialog(null,"Debe seleccionar un avion",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else if(this.agregarWindow.noHayOrigen(origenSeleccionado)){
            JOptionPane.showMessageDialog(null,"Debe seleccionar un origen",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else if(this.agregarWindow.noHayDestino(destinoSeleccionado)){
            JOptionPane.showMessageDialog(null,"Debe seleccionar un destino",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else{
            this.agregarVueloCharter();
            this.close();
        }
    }

    private void agregarVueloCharter() {

        Empresa.empresaUnica().cambiarCriterio(this.agregarWindow.getCriterio());
        Vuelo v1 = new VueloCharter(this.avionSeleccionado.getAvionSeleccionado(), this.getCantAsientosOcupados());
        v1.setOrigen(this.origenSeleccionado.getOrigenSeleccionado());
        v1.setDestino(this.destinoSeleccionado.getDestinoSeleccionado());
        v1.setPoliticaDePrecio(agregarWindow.getPolitica());
        v1.setFecha(LocalDate.of(this.agregarWindow.getAnio(),this.agregarWindow.getMes(), this.agregarWindow.getDia()));
        VueloStore.store().addVuelo(v1);

    }


    public int getCantAsientosOcupados() {
        return cantAsientosOcupados;
    }

    public void setCantAsientosOcupados(int cantAsientosOcupados) {
        this.cantAsientosOcupados = cantAsientosOcupados;
    }
}
