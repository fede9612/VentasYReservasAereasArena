package ventanas;

import org.uqbar.arena.bindings.ObservableProperty;
import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.GroupPanel;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.tables.Column;
import org.uqbar.arena.widgets.tables.Table;
import org.uqbar.arena.windows.MainWindow;

import vuelos.Vuelo;
import vuelos.VueloNormal;
import vuelos.VueloStore;
import windowsController.VueloController;

import java.awt.*;

public class VuelosDisponiblesWindow extends MainWindow<VueloStore>{

	private Boolean mostreMensaje = false;

	VueloController vueloController = new VueloController();
	
	public VuelosDisponiblesWindow(VueloStore model) {
		super(model);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void createContents(Panel panel) {

		new Label(panel)
				.setText("Vuelos disponibles")
				.setFontSize(12)
				.setForeground(Color.blue);

		this.tablaDeVuelos(panel);
		
		GroupPanel panelInfoAvion = new GroupPanel(panel, vueloController);
		panelInfoAvion.setTitle("Informaci�n del avion");
		panelInfoAvion.setLayout(new ColumnLayout(4));

		this.armarInfoAvion(panelInfoAvion, "vueloSeleccionado.avion");

		Panel panelDetalle = new Panel(panel, vueloController);

		Button detalle = new Button(panelDetalle);
			detalle.setCaption("Ver detalle");
			detalle.setWidth(1000);
			detalle.onClick(() ->{
				this.abrirDetallesDelVuelo();
			});

		Button agregarVueloBtn = new Button(panel);
		agregarVueloBtn.setCaption("Agregar Vuelo");
		agregarVueloBtn.setWidth(1000);
		agregarVueloBtn.onClick(() -> {
			new AgregarVuelosWindow(this, new VueloNormal(), this).open();
		});
	}

	private void abrirDetallesDelVuelo() {
		DetalleVueloWindows detalleVuelo = new DetalleVueloWindows(this,
                vueloController.getVueloSeleccionado(),this);
		detalleVuelo.open();
	}

	private void tablaDeVuelos(Panel panel) {
		Table<Vuelo> tableVuelos = new Table<>(panel, Vuelo.class);
		tableVuelos.bindItemsToProperty("vuelos");
		tableVuelos.setNumberVisibleRows(4);
		tableVuelos.bindValue(new ObservableProperty<>(vueloController, "vueloSeleccionado"));

		new Column<Vuelo>(tableVuelos)
			.setFixedSize(90)
			.setTitle("Fecha")
			.bindContentsToProperty("fechaDelViaje");
		new Column<Vuelo>(tableVuelos)
			.setFixedSize(90)
			.setTitle("Tipo")
			.bindContentsToProperty("tipo");
		new Column<Vuelo>(tableVuelos)
			.setFixedSize(90)
			.setTitle("Origen")
			.bindContentsToProperty("origen.nombre");
		new Column<Vuelo>(tableVuelos)
			.setFixedSize(90)
			.setTitle("Destino")
			.bindContentsToProperty("destino.nombre");
		new Column<Vuelo>(tableVuelos)
			.setFixedSize(110)
			.setTitle("Asientos Libres")
			.bindContentsToProperty("cantDeAsientosLibres");
		new Column<Vuelo>(tableVuelos)
			.setFixedSize(90)
			.setTitle("Precio Actual")
			.bindContentsToProperty("precio");
		new Column<Vuelo>(tableVuelos)
			.setFixedSize(90)
			.setTitle("Disponible la venta")
			.bindContentsToProperty("puedeVenderse");
	}

	public void armarInfoAvion(GroupPanel panel, String modelo){

		new Label(panel)
				.setFontSize(15)
				.setForeground(Color.gray)
				.bindValueToProperty(modelo + ".nombre");
		new Label(panel)
				.setText("");
		new Label(panel)
				.setText("");
		new Label(panel)
				.setText("");
		new Label(panel)
				.setText("Cantidad de asientos:		")
				.setForeground(Color.green);
		new Label(panel)
				.bindValueToProperty(modelo + ".cantAsientos");
		new Label(panel)
				.setText("Altura de la cabina: ")
				.setForeground(Color.green);
		new Label(panel)
				.bindValueToProperty(modelo + ".alturaDeCabina");
		new Label(panel)
				.setText("Peso del Avi�n:		")
				.setForeground(Color.green);
		new Label(panel)
				.bindValueToProperty(modelo + ".peso");
		new Label(panel)
				.setText("Consumo de nafta por Kilometros:		")
				.setForeground(Color.green);
		new Label(panel)
				.bindValueToProperty(modelo + ".consumoNafta");

	}

	public Boolean getMostreMensaje() {
		return mostreMensaje;
	}

	public void setMostreMensaje(Boolean mostreMensaje) {
		this.mostreMensaje = mostreMensaje;
	}
}
