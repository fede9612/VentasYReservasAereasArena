package ventanas;

import empresa.Empresa;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.TextBox;
import org.uqbar.arena.windows.Window;
import org.uqbar.arena.windows.WindowOwner;
import vuelos.*;
import windowsController.AvionController;
import windowsController.DestinoController;
import windowsController.OrigenController;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;

public class AgregarVueloWindows extends Window<Vuelo>{

    private AvionController avionSeleccionado = new AvionController();
    private DestinoController destinoSeleccionado = new DestinoController();
    private OrigenController origenSeleccionado = new OrigenController();
    private AgregarVuelosWindow agregarWindow;


    public AgregarVueloWindows(WindowOwner owner, Vuelo model, AgregarVuelosWindow window) {
        super(owner, model);
        this.agregarWindow = window;
    }

    @Override
    public void createContents(Panel mainPanel) {
        new Label(mainPanel)
                .setText("Agregar nuevo Vuelo Normal")
                .setFontSize(12)
                .setForeground(Color.blue);

        agregarWindow.panelFechas(mainPanel);

        agregarWindow.panelSeleccionarAvion(mainPanel, avionSeleccionado);

        agregarWindow.origenYDestinoPanel(mainPanel, origenSeleccionado, destinoSeleccionado);

        Button aceptarBtn = new Button(mainPanel);
        aceptarBtn.setCaption("Aceptar");
        aceptarBtn.onClick(() ->{
            this.chequeoYAgregaVueloNormal();
        });

    }

    private void chequeoYAgregaVueloNormal() {
        if(this.agregarWindow.noHayFechas()){
            JOptionPane.showMessageDialog(null,"Debe ingresar la fecha",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else if(this.agregarWindow.noHayAvion(avionSeleccionado)){
            JOptionPane.showMessageDialog(null,"Debe seleccionar un avion",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else if(this.agregarWindow.noHayOrigen(origenSeleccionado)){
            JOptionPane.showMessageDialog(null,"Debe seleccionar un origen",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else if(this.agregarWindow.noHayDestino(destinoSeleccionado)){
            JOptionPane.showMessageDialog(null,"Debe seleccionar un destino",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else{
            this.agregarVueloNormal();
            this.close();
        }
    }


    private void agregarVueloNormal() {
        
        Empresa.empresaUnica().cambiarCriterio(this.agregarWindow.getCriterio());
        Vuelo v1 = new VueloNormal(this.avionSeleccionado.getAvionSeleccionado());
        v1.setOrigen(this.origenSeleccionado.getOrigenSeleccionado());
        v1.setDestino(this.destinoSeleccionado.getDestinoSeleccionado());
        v1.setPoliticaDePrecio(agregarWindow.getPolitica());
        v1.setFecha(LocalDate.of(this.agregarWindow.getAnio(),this.agregarWindow.getMes(), this.agregarWindow.getDia()));
        VueloStore.store().addVuelo(v1);

    }


}
