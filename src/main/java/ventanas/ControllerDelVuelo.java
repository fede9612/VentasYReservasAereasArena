package ventanas;

import org.uqbar.commons.utils.Observable;
import vuelos.Vuelo;

@Observable
public class ControllerDelVuelo {

    private Vuelo componenteSeleccionado;


    public Vuelo getComponenteSeleccionado() {
        return componenteSeleccionado;
    }

    public void setComponenteSeleccionado(Vuelo componenteSeleccionado) {
        this.componenteSeleccionado = componenteSeleccionado;
    }
}
