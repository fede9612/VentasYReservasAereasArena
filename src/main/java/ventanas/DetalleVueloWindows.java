package ventanas;

import org.uqbar.arena.bindings.ObservableProperty;
import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.GroupPanel;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.tables.Column;
import org.uqbar.arena.widgets.tables.Table;
import org.uqbar.arena.windows.Window;
import org.uqbar.arena.windows.WindowOwner;

import org.uqbar.commons.model.ObservableUtils;
import pasajeroPersona.Pasajero;
import ventaDelPasaje.Venta;
import vuelos.Vuelo;
import windowsController.VentaController;

import java.awt.*;

public class DetalleVueloWindows extends Window<Vuelo>{

	private VuelosDisponiblesWindow windowOwner;
	private VentaController ventaSeleccionada = new VentaController();

	public DetalleVueloWindows(WindowOwner owner, Vuelo model, VuelosDisponiblesWindow windowOwner) {
		super(owner, model);
		this.windowOwner = windowOwner;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void createContents(Panel panel) {

		new Label(panel)
				.setText("Detalle del Vuelo")
				.setFontSize(12)
				.setForeground(Color.blue);

		this.detallesVuelo(panel);

		GroupPanel panelAvion = new GroupPanel(panel);
		panelAvion.setLayout(new ColumnLayout(4));
		panelAvion.setTitle("Información del Avion ");

		this.windowOwner.armarInfoAvion(panelAvion, "avion");

		tablaDePasajeros(panel);

		Button buttonAgregarPasajero = new Button(panel);
		buttonAgregarPasajero.setCaption("Agregar Pasajero");
		if(!this.getModelObject().verificarDisponibilidad()){
			buttonAgregarPasajero.disableOnError();
		}
		buttonAgregarPasajero.onClick(()->{
			this.abrirVentanaAgregarPasajeros();

		});

		Button buttonPagar = new Button(panel);
		buttonPagar.setCaption("Pagar");
		buttonPagar.onClick(() -> {
			this.abrirVentanaDePagos();
		});

	}

	private void abrirVentanaDePagos() {
		PagarPasajeWindow windowPagar = new PagarPasajeWindow(this,
                ventaSeleccionada.getVentaSeleccionada(), this);
		windowPagar.open();
	}

	private void abrirVentanaAgregarPasajeros() {
		AgregarPasajeroWindow windows = new AgregarPasajeroWindow(this,
                new Pasajero(),this.getModelObject(),
                this);
		windows.open();
	}

	private void tablaDePasajeros(Panel panel) {
		new Label(panel)
				.setText("Pasajeros/as")
				.setForeground(Color.gray)
				.setFontSize(14);


		Table<Venta> tablaPasajeros = new Table<>(panel, Venta.class);
		tablaPasajeros.bindItemsToProperty("ventas");
		tablaPasajeros.setNumberVisibleRows(4);
		tablaPasajeros.bindValue(new ObservableProperty<>(this.ventaSeleccionada, "ventaSeleccionada"));

		new Column<Venta>(tablaPasajeros)
				.setFixedSize(200)
				.setTitle("DNI")
				.bindContentsToProperty("pasajero.dni");
		new Column<Venta>(tablaPasajeros)
				.setFixedSize(120)
				.setTitle("Nombre")
				.bindContentsToProperty("pasajero.nomApe");
		new Column<Venta>(tablaPasajeros)
				.setFixedSize(200)
				.setTitle("Fecha de compra")
				.bindContentsToProperty("fechaDeCompra");
		new Column<Venta>(tablaPasajeros)
				.setFixedSize(200)
				.setTitle("Abonado")
				.bindContentsToProperty("totalDePagos");
		new Column<Venta>(tablaPasajeros)
				.setFixedSize(120)
				.setTitle("Falta pagar")
				.bindContentsToProperty("deudaDeUnaPersona");
	}

	private void detallesVuelo(Panel panel) {
		panel.setWidth(500);

		GroupPanel panelVuelo = new GroupPanel(panel);
		panelVuelo.setLayout(new ColumnLayout(4));
		panelVuelo.setTitle("Información del vuelo ");
		new Label(panelVuelo)
				.setText("Fecha:				")
				.setForeground(Color.green);
		new Label(panelVuelo)
				.bindValueToProperty("fechaDelViaje");
		new Label(panelVuelo)
				.setText("Origen:				")
				.setForeground(Color.green);
		new Label(panelVuelo)
				.bindValueToProperty("origen.nombre");
		new Label(panelVuelo)
				.setText("Destino:				")
				.setForeground(Color.green);
		new Label(panelVuelo)
				.bindValueToProperty("destino.nombre");
		new Label(panelVuelo)
				.setText("Tipo:				")
				.setForeground(Color.green);
		new Label(panelVuelo)
				.bindValueToProperty("tipo");
		new Label(panelVuelo)
				.setText("Asientos Libres:				")
				.setForeground(Color.green);
		new Label(panelVuelo)
				.bindValueToProperty("cantDeAsientosLibres");
		new Label(panelVuelo)
				.setText("Precio Actual:				")
				.setForeground(Color.green);
		new Label(panelVuelo)
				.bindValueToProperty("precio");
		new Label(panelVuelo)
				.setText("Disponible para la venta:				")
				.setForeground(Color.green);
		new Label(panelVuelo)
				.bindValueToProperty("puedeVenderse");
	}

	public void actualizarEstados(){
		ObservableUtils.firePropertyChanged(this.getModelObject(), "cantDeAsientosLibres");
		ObservableUtils.firePropertyChanged(this.ventaSeleccionada.getVentaSeleccionada(), "totalDePagos");
	}

}
