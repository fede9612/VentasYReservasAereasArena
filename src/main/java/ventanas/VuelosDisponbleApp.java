package ventanas;
import java.time.LocalDate;
import java.time.Month;

import avion.Avion;
import avion.AvionStore;
import criterioDeVentaPasajes.CriterioSegura;
import empresa.Empresa;
import pasaje.Pasaje;
import pasajeroPersona.Pasajero;
import politicaPrecioAsientosParaVuelo.PoliticaEstricta;
import ventaDelPasaje.Pago;
import ventaDelPasaje.Venta;
import vuelos.Ciudad;
import vuelos.Vuelo;
import vuelos.VueloNormal;
import vuelos.VueloStore;

public class VuelosDisponbleApp {
	
	public static void main(String[] args) {
		//Se crea 2 vuelos
		Empresa.empresaUnica().cambiarCriterio(new CriterioSegura());
		Avion n345 = new Avion(90, 3);
		n345.setPeso(30000);
		n345.setNombre("N345");
		n345.setConsumo(700);
		Vuelo vueloBarelona = new VueloNormal(n345);
		vueloBarelona.setPoliticaDePrecio(new PoliticaEstricta(5200));
		vueloBarelona.setOrigen(Ciudad.BsAs);
		vueloBarelona.setDestino(Ciudad.Barcelona);
		vueloBarelona.setFecha(LocalDate.of(2018, Month.OCTOBER, 15));
		Pasajero julian = new Pasajero("39146980");
		julian.setNomApe("Julian Gomez");
		Pasajero sabrina = new Pasajero("58741896");
		sabrina.setNomApe("Sabrina Gutierrez");
		Venta ventabarcelona = new Venta(vueloBarelona,new Pasaje(vueloBarelona),julian);
		ventabarcelona.setFechaDeCompra(LocalDate.of(2016, Month.APRIL, 23));
		ventabarcelona.agregarPago(new Pago(1000));
		Venta venta2barcelona = new Venta(vueloBarelona,new Pasaje(vueloBarelona),sabrina);
		venta2barcelona.setFechaDeCompra(LocalDate.of(2017, Month.JULY, 14));
		venta2barcelona.agregarPago(new Pago(2570));
		
		Avion boeing747 = new Avion(200, 5.5);
		boeing747.setPeso(20000);
		boeing747.setNombre("Boeing 747-8");
		boeing747.setConsumo(1480);
		Vuelo vueloBuenosAires = new VueloNormal(boeing747);
		vueloBuenosAires.setPoliticaDePrecio(new PoliticaEstricta(7500));
		vueloBuenosAires.setOrigen(Ciudad.LosAngeles);
		vueloBuenosAires.setDestino(Ciudad.BsAs);
		vueloBuenosAires.setFecha(LocalDate.of(2018, Month.OCTOBER, 29));
		Pasajero alberto = new Pasajero("45879652");
		alberto.setNomApe("Alberto Farias");
		Pasajero rodrigo = new Pasajero("85749685");
		rodrigo.setNomApe("Rodrigo Tomas Perez");

		Venta ventaBuenosAires = new Venta(vueloBuenosAires,new Pasaje(vueloBuenosAires),alberto);
		ventaBuenosAires.setFechaDeCompra(LocalDate.of(2016,Month.JULY,10));
		Venta venta2BuenosAires = new Venta(vueloBuenosAires, new Pasaje(vueloBuenosAires),rodrigo);
		venta2BuenosAires.setFechaDeCompra(LocalDate.of(2017, Month.APRIL, 5));
		venta2barcelona.agregarPago(new Pago(1000));
		VueloStore.store().addVuelo(vueloBarelona);
		VueloStore.store().addVuelo(vueloBuenosAires);

		//Crear aviones
		Avion avion1 = new Avion(100,2.5);
		avion1.setNombre("N322");
		avion1.setPeso(80000);
		avion1.setConsumo(200);

		Avion avion2 = new Avion(320, 6.0);
		avion2.setNombre("L22");
		avion2.setPeso(120000);
		avion2.setConsumo(300);
		AvionStore.avionStore().setAviones(avion1);
		AvionStore.avionStore().setAviones(avion2);

		Avion avion3 = new Avion(150, 8.0);
		avion3.setNombre("A380");
		avion3.setPeso(150000);
		avion3.setConsumo(600);
		AvionStore.avionStore().setAviones(avion3);
		
		VuelosDisponiblesWindow window = new VuelosDisponiblesWindow(VueloStore.store());
		window.startApplication();
	}
}
