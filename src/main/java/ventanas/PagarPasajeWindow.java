package ventanas;


import org.uqbar.arena.bindings.ObservableProperty;
import org.uqbar.arena.filters.TextFilter;
import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.TextBox;
import org.uqbar.arena.windows.Window;
import org.uqbar.arena.windows.WindowOwner;
import ventaDelPasaje.Pago;
import ventaDelPasaje.Venta;

import javax.swing.*;
import java.awt.*;

public class PagarPasajeWindow extends Window<Venta>{

    private Pago pago = new Pago();
    private DetalleVueloWindows windowDetalle;

    public PagarPasajeWindow(WindowOwner owner, Venta model, DetalleVueloWindows window) {
        super(owner, model);
        this.windowDetalle = window;
    }

    @Override
    public void createContents(Panel mainPanel) {

        new Label(mainPanel)
                .setText("Pago de Pasaje")
                .setFontSize(12)
                .setForeground(Color.blue);

        Panel panelTitle = new Panel(mainPanel);
        panelTitle.setLayout(new ColumnLayout(2));

        this.datosDePago(panelTitle);

        Button aceptarBtn = new Button(mainPanel);
        aceptarBtn.setCaption("Pagar");
        aceptarBtn.setWidth(60);
        aceptarBtn.onClick(() -> {
            this.chequeoYPago();
        });

        Button cancelarBtn = new Button(mainPanel);
        cancelarBtn.setCaption("Cancelar");
        cancelarBtn.onClick(() -> this.close());
    }

    private void datosDePago(Panel panelTitle) {
        new Label(panelTitle)
                .setText("Monto a pagar");

        new Label(panelTitle)
                .bindValueToProperty("deudaDeUnaPersona");

        new Label(panelTitle)
                .setText("Monto de pago");
        TextBox txtMonto = new TextBox(panelTitle);
        txtMonto.setWidth(70);
        txtMonto.withFilter(TextFilter.NUMERIC_TEXT_FILTER);
        txtMonto.bindValue(new ObservableProperty<Object>(this.pago, "montoDelPago"));
    }

    private void chequeoYPago() {
        if(this.pagoElTotal()){
            JOptionPane.showMessageDialog(null,"No puede pagar m�s del monto a pagar",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }else if(this.noQuierePagar()){
            JOptionPane.showMessageDialog(null,"No puede pagar 0 pesos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
        else{
            this.getModelObject().agregarPago(this.pago);
            windowDetalle.actualizarEstados();
            this.close();
        }
    }

    private boolean noQuierePagar() {
        return this.pago.getMontoDelPago() == 0;
    }

    private boolean pagoElTotal() {
        return this.pago.getMontoDelPago() > this.getModelObject().getDeudaDeUnaPersona();
    }
}
