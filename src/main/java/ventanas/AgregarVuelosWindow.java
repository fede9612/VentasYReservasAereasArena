package ventanas;


import avion.Avion;
import avion.AvionStore;
import criterioDeVentaPasajes.Criterio;
import criterioDeVentaPasajes.CriterioLaxa;
import criterioDeVentaPasajes.CriterioPorcentaje;
import criterioDeVentaPasajes.CriterioSegura;
import org.uqbar.arena.bindings.ObservableProperty;
import org.uqbar.arena.filters.TextFilter;
import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.widgets.*;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.tables.Column;
import org.uqbar.arena.widgets.tables.Table;
import org.uqbar.arena.windows.Window;
import org.uqbar.arena.windows.WindowOwner;
import politicaPrecioAsientosParaVuelo.PoliticaDePrecio;
import politicaPrecioAsientosParaVuelo.PoliticaEstricta;
import politicaPrecioAsientosParaVuelo.PoliticaRemate;
import politicaPrecioAsientosParaVuelo.PoliticaVentaAnticipada;
import vuelos.*;
import windowsController.AvionController;
import windowsController.DestinoController;
import windowsController.OrigenController;

import javax.swing.*;
import java.awt.*;

public class AgregarVuelosWindow extends Window<Vuelo>{

    private VuelosDisponiblesWindow vuelosDisponiblesWindow;
    private PoliticaDePrecio politica;
    private int precio;
    private Criterio criterio;
    private Integer dia = 0;
    private Integer mes = 0;
    private Integer anio = 0;

    public AgregarVuelosWindow(WindowOwner owner, Vuelo model, VuelosDisponiblesWindow window) {
        super(owner, model);
        this.vuelosDisponiblesWindow = window;
    }

    @Override
    public void createContents(Panel mainPanel) {
        if(!vuelosDisponiblesWindow.getMostreMensaje()){
            vuelosDisponiblesWindow.setMostreMensaje(true);
            invocarAdvertencia();
        }

        new Label(mainPanel)
                .setText("Agregar nuevo Vuelo")
                .setFontSize(12)
                .setForeground(Color.blue);

        Panel panelSecundario = new Panel(mainPanel);
        panelSecundario.setLayout(new ColumnLayout(2));

        new Label(panelSecundario)
                .setText("Precio");

        TextBox txtPrecio = new TextBox(panelSecundario);
        txtPrecio.withFilter(TextFilter.NUMERIC_TEXT_FILTER);
        txtPrecio.setWidth(100);
        txtPrecio.bindValue(new ObservableProperty<Object>(this, "precio"));

        GroupPanel panelDatos = new GroupPanel(mainPanel);
        panelDatos.setTitle("Seleccion de datos");

        this.panelSeleccionarPolitica(panelDatos);

        this.panelSeleccionarCriterio(panelDatos);

        this.panelSeleccionarTipoDeVuelo(panelDatos);
    }

    private void panelSeleccionarTipoDeVuelo(GroupPanel panelDatos) {
        new Label(panelDatos)
                .setText("Seleccione un tipo de vuelo")
                .setForeground(Color.BLUE)
                .setFontSize(12);

        GroupPanel panelVuelos = new GroupPanel(panelDatos);
        panelVuelos.setTitle("Tipos");
        panelVuelos.setLayout(new ColumnLayout(3));

        Button vueloNormalBtn = new Button(panelVuelos);
        vueloNormalBtn.setCaption("Vuelo Normal");
        vueloNormalBtn.onClick(() -> {
            this.chequearYAbrirVentanaVueloNormal();
        });

        Button vueloCharterBtn = new Button(panelVuelos);
        vueloCharterBtn.setCaption("Vuelo Charter");
        vueloCharterBtn.onClick(() -> {
            this.chequearYAbrirVentanaVueloCharter();
        });

        Button vueloCargaBtn = new Button(panelVuelos);
        vueloCargaBtn.setCaption("Vuelo de Carga");
        vueloCargaBtn.onClick(() -> {
            this.chequearYAbrirVentanaVueloDeCarga();
        });
    }

    private void panelSeleccionarCriterio(GroupPanel panelDatos) {
        new Label(panelDatos)
                .setText("Seleccione un criterio")
                .setForeground(Color.BLUE)
                .setFontSize(12);

        GroupPanel panelCriterios = new GroupPanel(panelDatos);
        panelCriterios.setTitle("Criterios");
        panelCriterios.setLayout(new ColumnLayout(3));

        Button criterioSeguraBtn = new Button(panelCriterios);
        criterioSeguraBtn.setCaption("Seguro");
        criterioSeguraBtn.onClick(() -> {
            this.criterio = new CriterioSegura();
        });

        Button criterioLaxaBtn = new Button(panelCriterios);
        criterioLaxaBtn.setCaption("Laxa");
        criterioLaxaBtn.onClick(() -> {
            this.criterio = new CriterioLaxa();
        });

        Button criterioPorcentajeBtn = new Button(panelCriterios);
        criterioPorcentajeBtn.setCaption("Porcentaje");
        criterioPorcentajeBtn.onClick(() -> {
            this.criterio = new CriterioPorcentaje();
        });
    }

    private void panelSeleccionarPolitica(GroupPanel panelDatos) {
        new Label(panelDatos)
                .setText("Seleccione una politica")
                .setForeground(Color.BLUE)
                .setFontSize(12);

        GroupPanel panelPoliticas = new GroupPanel(panelDatos);
        panelPoliticas.setTitle("Politicas");
        panelPoliticas.setLayout(new ColumnLayout(3));

        Button politicaEstrictaBtn = new Button(panelPoliticas);
        politicaEstrictaBtn.setCaption("Estricta");
        politicaEstrictaBtn.onClick(() -> {
            politica = new PoliticaEstricta(this.getPrecio());
        });

        Button politicaRemateBtn = new Button(panelPoliticas);
        politicaRemateBtn.setCaption("Remate");
        politicaRemateBtn.onClick(() -> {
            politica = new PoliticaRemate(this.getPrecio());
        });

        Button politicaAnticipadaBtn = new Button(panelPoliticas);
        politicaAnticipadaBtn.setCaption("Anticipada");
        politicaAnticipadaBtn.onClick(() -> {
            politica = new PoliticaVentaAnticipada(this.getPrecio());
        });
    }

    private void chequearYAbrirVentanaVueloDeCarga() {
        if(this.noHayPrecio()){
            JOptionPane.showMessageDialog(null,"Debe ingresar el precio",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else if(this.noHayPolitica()){
            JOptionPane.showMessageDialog(null,"Debe seleccionar una politica",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else if(this.noHayCriterio()){
            JOptionPane.showMessageDialog(null,"Debe seleccionar un criterio",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else {
            new AgregarVueloCargaWindow(this, new VueloDeCarga(), this).open();
            this.close();
        }
    }

    private void chequearYAbrirVentanaVueloCharter() {
        if(this.noHayPrecio()){
            JOptionPane.showMessageDialog(null,"Debe ingresar el precio",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else if(this.noHayPolitica()){
            JOptionPane.showMessageDialog(null,"Debe seleccionar una politica",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else if(this.noHayCriterio()){
            JOptionPane.showMessageDialog(null,"Debe seleccionar un criterio",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else{
            new AgregarVueloCharterWindow(this, new VueloCharter(), this).open();
            this.close();
        }
    }

    private void chequearYAbrirVentanaVueloNormal() {
        if(this.noHayPrecio()){
            JOptionPane.showMessageDialog(null,"Debe ingresar el precio",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else if(this.noHayPolitica()){
            JOptionPane.showMessageDialog(null,"Debe seleccionar una politica",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else if(this.noHayCriterio()){
            JOptionPane.showMessageDialog(null,"Debe seleccionar un criterio",
                    "Error",JOptionPane.INFORMATION_MESSAGE);
        }else{
            new AgregarVueloWindows(this, new VueloNormal(), this).open();
            this.close();
        }
    }

    private boolean noHayCriterio() {
        return this.criterio == null;
    }

    private boolean noHayPolitica() {
        return this.politica == null;
    }

    private boolean noHayPrecio() {
        return this.getPrecioString() == null || this.getPrecio() == 0;
    }

    private void invocarAdvertencia() {
        JOptionPane.showMessageDialog(null,"Seleccionar los datos de manera decreciente",
                "Atenci�n",JOptionPane.INFORMATION_MESSAGE);
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public PoliticaDePrecio getPolitica() {
        return politica;
    }

    public Criterio getCriterio() {
        return criterio;
    }

    public void panelFechas(Panel mainPanel) {

        Panel panelFecha = new Panel(mainPanel);
        panelFecha.setLayout(new ColumnLayout(7));

        new Label(panelFecha)
                .setText("Fecha: ");
        new Label(panelFecha)
                .setText("D�a")
                .setForeground(Color.gray);
        TextBox txtDia = new TextBox(panelFecha);
        txtDia.setWidth(50);
        txtDia.withFilter(TextFilter.NUMERIC_TEXT_FILTER);
        txtDia.bindValue(new ObservableProperty<Object>(this, "dia"));
        new Label(panelFecha)
                .setText("Mes")
                .setForeground(Color.gray);
        TextBox txtMes = new TextBox(panelFecha);
        txtMes.setWidth(50);
        txtMes.withFilter(TextFilter.NUMERIC_TEXT_FILTER);
        txtMes.bindValue(new ObservableProperty<Object>(this, "mes"));
        new Label(panelFecha)
                .setText("A�o")
                .setForeground(Color.gray);
        TextBox txtAnio = new TextBox(panelFecha);
        txtAnio.setWidth(50);
        txtAnio.withFilter(TextFilter.NUMERIC_TEXT_FILTER);
        txtAnio.bindValue(new ObservableProperty<Object>(this, "anio"));

    }

    public void panelSeleccionarAvion(Panel mainPanel, AvionController avionSeleccionado) {

        new Label(mainPanel)
                .setText("Seleccione un avion")
                .setForeground(Color.gray);

        Table<Avion> tableAviones = new Table<>(mainPanel,Avion.class);
        tableAviones.bindItems(new ObservableProperty<Object>(AvionStore.avionStore(),"aviones"));
        tableAviones.setNumberVisibleRows(3);
        tableAviones.bindValue(new ObservableProperty<Object>(avionSeleccionado,"avionSeleccionado"));

        new Column<Avion>(tableAviones)
                .setTitle("Nombre")
                .bindContentsToProperty("nombre");
        new Column<Avion>(tableAviones)
                .setTitle("Asientos")
                .bindContentsToProperty("cantAsientos");
        new Column<Avion>(tableAviones)
                .setTitle("Peso")
                .bindContentsToProperty("peso");

        new Label(mainPanel)
                .setText("Seleccione un destino")
                .setForeground(Color.gray);

    }

    public void origenYDestinoPanel(Panel mainPanel, OrigenController origenSeleccionado, DestinoController destinoSeleccionado) {
        Table<Ciudad> tableDestino = new Table<>(mainPanel, Ciudad.class);
        tableDestino.bindItems(new ObservableProperty<Object>(CiudadStore.store(), "ciudades"));
        tableDestino.setNumberVisibleRows(3);
        tableDestino.bindValue(new ObservableProperty<Object>(destinoSeleccionado, "destinoSeleccionado"));

        new Column<Ciudad>(tableDestino)
                .setTitle("Destino")
                .bindContentsToProperty("nombre");

        new Label(mainPanel)
                .setText("Seleccione un origen")
                .setForeground(Color.gray);

        Table<Ciudad> tableOrigen = new Table<>(mainPanel, Ciudad.class);
        tableOrigen.bindItems(new ObservableProperty<Object>(CiudadStore.store(), "ciudades"));
        tableOrigen.setNumberVisibleRows(3);
        tableOrigen.bindValue(new ObservableProperty<Object>(origenSeleccionado, "origenSeleccionado"));

        new Column<Ciudad>(tableOrigen)
                .setTitle("Origen")
                .bindContentsToProperty("nombre");
    }

    public Integer getDia() {
        return dia;
    }

    public void setDia(Integer dia) {
        this.dia = dia;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    private String getPrecioString(){
        return String.valueOf(this.precio);
    }

    public String getDiaString(){
        return String.valueOf(this.getDia());
    }

    public boolean noHayDestino(DestinoController destino) {
        return destino.getDestinoSeleccionado() == null;
    }

    public boolean noHayOrigen(OrigenController origen) {
        return origen.getOrigenSeleccionado() == null;
    }

    public boolean noHayAvion(AvionController avion) {
        return avion.getAvionSeleccionado() == null;
    }

    public boolean noHayFechas() {
        return this.getAnio() == 0 || this.getMes() == 0 || this.getDia() == 0;
    }
}
